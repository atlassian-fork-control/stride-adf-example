let express = require("express");
let router = express.Router();

const jwtUtil = require('jwt-simple');

var { Document } = require('adf-builder');

var messages = require('../helper/messages');

/**
 * Return an advanced application card.
 */
router.get('/',
function(req, res){
  const encodedJWT = req.headers["authorization"].substring(7); 
  const jwt = jwtUtil.decode(encodedJWT, null, true);
  const cloudId = jwt.context.cloudId;
  const conversationId = jwt.context.resourceId;
  var doc = new Document();

  var card = doc.applicationCard('With a title')
        .link('https://www.atlassian.com')
        .description('With some description, and a couple of attributes')
        .background('https://www.atlassian.com');
    card.detail()
        .title('Type')
        .text('Task')
        .icon({
          url: 'https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&avatarId=15318&avatarType=issuetype',
          label: 'Task'
        });
    card.detail()
        .title('User')
        .text('Joe Blog')
        .icon({
          url: 'https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&avatarId=15318&avatarType=issuetype',
          label: 'Task'
        });
    card.detail()
        .lozenge({
          text: 'Nearly Complete',
          appearance: 'inprogress'
        });
    card.detail()
        .badge({
          value: 101,
          max : 99,
          appearance: 'primary',
          theme: 'dark'
        });

    
  /**
   * Let's use JSON.stringify to get a nicely formatted JSON version of our example.
   */
  var code = JSON.stringify(doc, null, 4);

  /**
   * Now we'll reset our doc and create the message showing all the information.
   */
  doc = new Document();
  doc.rule();
  doc.heading(3)
  .text("Advanced application card example:");
  doc.rule();

  card = doc.applicationCard('With a title')
        .link('https://www.atlassian.com')
        .description('With some description, and a couple of attributes')
        .background('https://www.atlassian.com');
    card.detail()
        .title('Type')
        .text('Task')
        .icon({
          url: 'https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&avatarId=15318&avatarType=issuetype',
          label: 'Task'
        });
    card.detail()
        .title('User')
        .text('Joe Blog')
        .icon({
          url: 'https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&avatarId=15318&avatarType=issuetype',
          label: 'Task'
        });
    card.detail()
        .lozenge({
          text: 'Nearly Complete',
          appearance: 'inprogress'
        });
    card.detail()
        .badge({
          value: 101,
          max : 99,
          appearance: 'primary',
          theme: 'dark'
        });

  doc.rule();

  doc.paragraph()
    .text('Here is the JavaScript code to build the example above:');
  doc.codeBlock("javascript")
    .text('var { Document } = require(\'adf-builder\'); \
    \n const doc = new Document(); \
    \n const card = doc.applicationCard(\'With a title\') \
    \n    .link(\'https://www.atlassian.com\') \
    \n    .description(\'With some description, and a couple of attributes\') \
    \n    .background(\'https://www.atlassian.com\'); \
    \n card.detail() \
    \n    .title(\'Type\') \
    \n    .text(\'Task\') \
    \n    .icon({ \
    \n      url: \'https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&avatarId=15318&avatarType=issuetype\', \
    \n      label: \'Task\' \
    \n    }); \
    \n card.detail() \
    \n    .title(\'User\') \
    \n    .text(\'Joe Blog\') \
    \n    .icon({ \
    \n      url: \'https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&avatarId=15318&avatarType=issuetype\', \
    \n      label: \'Task\' \
    \n    }); \
    \n card.detail() \
    \n    .lozenge({ \
    \n      text: \'Nearly Complete\', \
    \n      appearance: \'inprogress\' \
    \n    }); \
    \n card.detail() \
    \n    .badge({ \
    \n      value: 101, \
    \n      max : 99, \
    \n      appearance: \'primary\', \
    \n      theme: \'dark\' \
    \n    }); \
    ');

  doc.paragraph()
    .text('... and here is the example message in JSON (ADF):');
  doc.codeBlock("javascript")
    .text(code);
  
  var reply = doc.toJSON();
  messages.sendMessage(cloudId, conversationId, reply, function (err, response) {
    if (err)
    console.log(err);
  });
  res.sendStatus(204);
});

module.exports = router;