let express = require("express");
let router = express.Router();

const jwtUtil = require('jwt-simple');

var { Document } = require('adf-builder');

var messages = require('../helper/messages');

/**
 * Return an advanced example.
 */
router.get('/', 
  function (req, res){
    const encodedJWT = req.headers["authorization"].substring(7); 
    const jwt = jwtUtil.decode(encodedJWT, null, true);
    const cloudId = jwt.context.cloudId;
    const conversationId = jwt.context.resourceId;
    var doc = new Document();
   
    doc.paragraph()
        .text('Here is some ')
        .strong('bold test')
        .text(' and ')
        .em('text in italics')
        .text(' as well as ')
        .link(' a link', 'https://www.atlassian.com')
        .text(' , emojis ')
        .emoji(':smile:')
        .emoji(':rofl:')
        .emoji(':nerd:')
        .text(' and some code: ')
        .code('var i = 0;')
        .text(' and a bullet list');
    doc.bulletList()
        .textItem('With one bullet point')
        .textItem('And another');
    doc.panel("info")
        .paragraph()
        .text("and an info panel with some text, with some more code below");
    doc.codeBlock("javascript")
        .text('var i = 0;\nwhile(true) {\n  i++;\n}');
    doc.paragraph()
        .text("And a card");
    var card = doc.applicationCard('With a title')
        .link('https://www.atlassian.com')
        .description('With some description, and a couple of attributes')
        .background('https://www.atlassian.com');
    card.detail()
        .title('Type')
        .text('Task')
        .icon({
          url: 'https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&avatarId=15318&avatarType=issuetype',
          label: 'Task'
        })
    card.detail()
        .title('User')
        .text('Joe Blog')
        .icon({
          url: 'https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&avatarId=15318&avatarType=issuetype',
          label: 'Task'
        });

    /**
     * Let's use JSON.stringify to get a nicely formatted JSON version of our example.
     */
    var code = JSON.stringify(doc, null, 4);

    /**
     * Now we'll reset our doc and create the message showing all the information.
     */
    doc = new Document();
    doc.rule();
    doc.heading(3)
        .text("Advanced All-in-One example:");
    doc.rule();  

    doc.paragraph()
        .text('Here is some ')
        .strong('bold test')
        .text(' and ')
        .em('text in italics')
        .text(' as well as ')
        .link(' a link', 'https://www.atlassian.com')
        .text(' , emojis ')
        .emoji(':smile:')
        .emoji(':rofl:')
        .emoji(':nerd:')
        .text(' and some code: ')
        .code('var i = 0;')
        .text(' and a bullet list');
    doc.bulletList()
        .textItem('With one bullet point')
        .textItem('And another');
    doc.panel("info")
        .paragraph()
        .text("and an info panel with some text, with some more code below");
    doc.codeBlock("javascript")
        .text('var i = 0;\nwhile(true) {\n  i++;\n}');
    doc.paragraph()
        .text("And a card");
    var card = doc.applicationCard('With a title')
        .link('https://www.atlassian.com')
        .description('With some description, and a couple of attributes')
        .background('https://www.atlassian.com');
    card.detail()
        .title('Type')
        .text('Task')
        .icon({
          url: 'https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&avatarId=15318&avatarType=issuetype',
          label: 'Task'
        })
    card.detail()
        .title('User')
        .text('Joe Blog')
        .icon({
          url: 'https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&avatarId=15318&avatarType=issuetype',
          label: 'Task'
        });

    doc.rule();
    doc.paragraph()
      .text('Here is the JavaScript code to build the example above:');
    doc.codeBlock("javascript")
      .text('var { Document } = require(\'adf-builder\');\
       \n doc.paragraph() \
       \n    .text(\'Here is some \') \
       \n    .strong(\'bold test\') \
       \n    .text(\' and \') \
       \n    .em(\'text in italics\') \
       \n    .text(\' as well as \') \
       \n    .link(\' a link\', \'https://www.atlassian.com\') \
       \n    .text(\' , emojis \') \
       \n    .emoji(\':smile:\') \
       \n    .emoji(\':rofl:\') \
       \n    .emoji(\':nerd:\') \
       \n    .text(\' and some code: \') \
       \n    .code(\'var i = 0;\') \
       \n    .text(\' and a bullet list\'); \
       \n \
       \n doc.bulletList() \
       \n    .textItem(\'With one bullet point\') \
       \n    .textItem(\'And another\'); \
       \n \
       \n doc.panel(\'info\') \
       \n    .paragraph() \
       \n    .text(\'and an info panel with some text, with some more code below\'); \
       \n \
       \n doc.codeBlock(\'javascript\') \
       \n    .text(\'var i = 0;\nwhile(true) {\n  i++;\n}\'); \
       \n \
       \n doc.paragraph() \
       \n    .text(\'And a card\'); \
       \n const card = doc.applicationCard(\'With a title\') \
       \n    .link(\'https://www.atlassian.com\') \
       \n    .description(\'With some description, and a couple of attributes\') \
       \n    .background(\'https://www.atlassian.com\'); \
       \n card.detail() \
       \n    .title(\'Type\') \
       \n    .text(\'Task\') \
       \n    .icon({ \
       \n       url: \'https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&avatarId=15318&avatarType=issuetype\', \
       \n       label: \'Task\' \
       \n    }) \
       \n card.detail() \
       \n    .title(\'User\') \
       \n    .text(\'Joe Blog\') \
       \n    .icon({ \
       \n       url: \'https://ecosystem.atlassian.net/secure/viewavatar?size=xsmall&avatarId=15318&avatarType=issuetype\', \
       \n       label: \'Task\' \
       \n    });\
       ');

    doc.paragraph()
       .text('... and here is the example message in JSON (ADF):');
    doc.codeBlock("javascript")
       .text(code);

    var reply = doc.toJSON();
    messages.sendMessage(cloudId, conversationId, reply, function (err, response) {
      if (err)
        console.log(err);
    });
    res.sendStatus(204);
  }
);

module.exports = router;