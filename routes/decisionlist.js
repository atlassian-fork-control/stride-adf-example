let express = require("express");
let router = express.Router();

const jwtUtil = require('jwt-simple');

var { Document } = require('adf-builder');

var messages = require('../helper/messages');

/**
 * Return a decision list.
 */
router.get('/',
function(req, res){
  const encodedJWT = req.headers["authorization"].substring(7); 
  const jwt = jwtUtil.decode(encodedJWT, null, true);
  const cloudId = jwt.context.cloudId;
  const conversationId = jwt.context.resourceId;
  var doc = new Document();

  var list = doc.decisionList('decision-id-1');
  list     
    .decision('test-id-1', 'DECIDED')        
    .text('Decided decision');

  list      
    .decision('test-id-2', 'UNDECIDED')        
    .text('Undecided decision');
     

  /**
   * Let's use JSON.stringify to get a nicely formatted JSON version of our example.
   */
  var code = JSON.stringify(doc, null, 4);

  /**
   * Now we'll reset our doc and create the message showing all the information.
   */
  doc = new Document();
  doc.rule();
  doc.heading(3)
    .text("Decision list example:");
  doc.rule(); 

  list = doc.decisionList('decision-id-1');
  list     
    .decision('test-id-1', 'DECIDED')        
    .text('Decided decision');

  list      
    .decision('test-id-2', 'UNDECIDED')        
    .text('Undecided decision');
  
  doc.rule();
  doc.paragraph()
    .text('Here is the JavaScript code to build the example above:');
  doc.codeBlock("javascript")
    .text('var { Document } = require(\'adf-builder\'); \
    \n const doc = new Document(); \
    \n const list = doc.decisionList(\'decision-id-1\'); \
    \n list   \
    \n    .decision(\'test-id-1\', \'DECIDED\') \
    \n    .text(\'Decided decision\'); \
    \n list \
    \n .decision(\'test-id-2\', \'UNDECIDED\') \
    \n .text(\'Undecided decision\'); \
    ');

  doc.paragraph()
    .text('... and here is the example message in JSON (ADF):');
  doc.codeBlock("javascript")
    .text(code);
  
  var reply = doc.toJSON();
  messages.sendMessage(cloudId, conversationId, reply, function (err, response) {
    if (err)
    console.log(err);
  });
  res.sendStatus(204);
});

module.exports = router;
