let express = require("express");
let router = express.Router();

const jwtUtil = require('jwt-simple');

var { Document } = require('adf-builder');

var messages = require('../helper/messages');

/**
 * Return a headings example.
 */
router.get('/',
function(req, res){
  const encodedJWT = req.headers["authorization"].substring(7); 
  const jwt = jwtUtil.decode(encodedJWT, null, true);
  const cloudId = jwt.context.cloudId;
  const conversationId = jwt.context.resourceId;
  var doc = new Document();

  doc.heading(1)
    .text("Heading level 1");
  doc.heading(2)
    .text("Heading level 2");
  doc.heading(3)
    .text("Heading level 3");
  doc.heading(4)
    .text("Heading level 4");
  doc.heading(5)
    .text("Heading level 5");
  doc.heading(6)
    .text("Heading level 6");


  /**
   * Let's use JSON.stringify to get a nicely formatted JSON version of our example.
   */
  var code = JSON.stringify(doc, null, 4);

  /**
   * Now we'll reset our doc and create the message showing all the information.
   */
  doc = new Document();
  doc.rule();
  doc.heading(3)
    .text("Heading example:");
  doc.rule();

  doc.heading(1)
    .text("Heading level 1");
  doc.heading(2)
    .text("Heading level 2");
  doc.heading(3)
    .text("Heading level 3");
  doc.heading(4)
    .text("Heading level 4");
  doc.heading(5)
    .text("Heading level 5");
  doc.heading(6)
    .text("Heading level 6");

  doc.rule();
  
  doc.paragraph()
    .text('Here is the JavaScript code to build the example above:');
  doc.codeBlock("javascript")
    .text(' var { Document } = require(\'adf-builder\'); \
    \n const doc = new Document(); \
    \n doc.heading(1) \
    \n   .text("Heading level 1"); \
    \n doc.heading(2) \
    \n   .text("Heading level 2"); \
    \n doc.heading(3) \
    \n   .text("Heading level 3"); \
    \n doc.heading(4) \
    \n   .text("Heading level 4"); \
    \n doc.heading(5) \
    \n   .text("Heading level 5"); \
    \n doc.heading(6) \
    \n   .text("Heading level 6"); \
    ');

  doc.paragraph()
    .text('... and here is the example message in JSON (ADF):');
  doc.codeBlock("javascript")
    .text(code);
  
  var reply = doc.toJSON();
  messages.sendMessage(cloudId, conversationId, reply, function (err, response) {
    if (err)
    console.log(err);
  });
  res.sendStatus(204);
});

module.exports = router;